#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void propagate(int generation) {
    if (generation <= 0) {
		printf("\n");
        exit(0);
    }
    printf("%d", generation);
    fflush(stdout);

    pid_t child_pid = fork();

	if (child_pid < 0) {
        perror("fork() error");
        exit(1);
    } else if (child_pid == 0) {
        propagate(generation - 1);  // Recursive call in child process
    } else {
        wait(NULL);
        exit(0);
    }
}

int main() {
    propagate(5);  // Start propagation from generation 1
    return 0;
}
