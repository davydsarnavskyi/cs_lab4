ASFLAGS = -g -F dwarf
LDFLAGS = -static

SRCS = exit.s fork.s
OBJS = $(SRCS:.s=.o)
EXECS = $(SRCS:.s=)

.PHONY: all clean

all: $(EXECS)

%: %.o
	ld $(LDFLAGS) -o $@ $<

%.o: %.s
	as $(ASFLAGS) -o $@ $<

clean:
	rm -f $(OBJS) $(EXECS)
